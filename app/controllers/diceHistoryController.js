// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Model
const diceHistoryModel = require('../models/diceHistoryModel');
const userModel = require('../models/userModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');

//  create Dice History
const createDiceHistory = (request, response) => {
    const username = request.query.username;
    const body = request.body;

    //
    let newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        dice: Math.floor(Math.random() * 6) + 1,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    if(username) {
        diceHistoryModel.create(newDiceHistory, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            userModel.findOneAndUpdate({username}, {
                $push: {
                    dicehistory: data._id
                }
            }, (err, updateUser) => {
                if(err) {
                    return response.status(500).json({
                        status: 'Error 500: Internal server error',
                        message: error.message
                    })
                }
                return response.status(201).json({
                    status: 'Create dice history successfully',
                    data: data
                })
            })
        })
    }
}

// Get All Dice History
const getAllDiceHistory = (request, response) => {
    let userId = request.query.userId;

    let condition = {};

    if(userId) {
        condition.userId = userId;
    }

    diceHistoryModel.find(condition)
        .exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Data',
                data: data
            })
        }
    })
}

// Get Dice History By Id
const getDiceHistoryById = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;

    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'diceHistoryId is invalid'
        })
    }

    diceHistoryModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get User',
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;
    const body = request.body;

    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'diceHistoryId is invalid'
        })
    }
    if(isNaN(body.dice) || body.dice < 0 && body.dice > 6){
        return response.status(400).json({
            status: "Bad Request",
            message:"dice is invalid"
        })
    }
    let updateDiceHistory = {
        user: body.user,
        dice: body.dice
    }
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

const deleteDiceHistory = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;

    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'diceHistoryId is invalid'
        })
    }
    diceHistoryModel.findByIdAndRemove(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
            })
        }
    })
}

const getDiceHistoryByUsername = (request, response) => {
    const username = request.query.username;

    if(username) {
        userModel.findOne({username}, (errorFindUser, userExist) => {
            if(errorFindUser) {
                return response.status(500).json({
                    status: 'Error 500: Internal server error',
                    message: error.message
                })
            } else {
                if(!userExist){
                    return response.status(500).json({
                        status: 'Username is not valid',
                        data: []
                    })
                } else {
                    userModel.findOne({username}, (error, data) => {
                        if(error) {
                            return response.status(500).json({
                                status: 'Internal server error',
                                message: error.message
                            })
                        } else {
                            return response.status(200).json({
                                status: `Get Dice History Of User ${username}`,
                                dicehistory: data.dicehistory
                            })
                        }
                    })
                }
            }
        })
    }

    diceHistoryModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Get all dice histories successfully',
                data: data
            })
        }
    })
}

const getVoucherHistoryByUsername = (request, response) => {
    const username = request.query.username;

    if(username) {
        userModel.findOne({username}, (errorFindUser, userExist) => {
            if(errorFindUser) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            } else {
                if(!userExist) {
                    return response.status(500).json({
                        status: 'Username is not valid',
                        data: []
                    })
                } else {
                    userModel.findOne({username}, (error, data) => {
                        if(error) {
                            return response.status(500).json({
                                status: 'Internal server error',
                                message: error.message
                            })
                        } else {
                            return response.status(200).json({
                                status: `Get Voucher History Of User ${userrname}`,
                                voucherhistory: data.voucherhistory
                            })
                        }
                    })
                }
            }
        })
    }

    voucherHistoryModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Get all voucher histories successfully',
                data: data
            })
        }
    })
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistory,
    getDiceHistoryByUsername,
    getVoucherHistoryByUsername
}