// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Prize Model
const prizeModel = require('../models/prizeModel');

// Create Prize
const createPrize = (request, response) => {
    const body = request.body;

    //
    if(!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is required'
        })
    }
    
    let newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    prizeModel.create(newPrize, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Created!',
                data: data
            })
        }
    })
}

// Get All Prize
const getAllPrize = (request, response) => {
    prizeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Data!',
                data: data
            })
        }
    })
}

// Get Prize By Id
 const getPrizeById = (request, response) => {
    const prizeId = request.params.prizeId;

    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'PrizeId is invalid'
        })
    }
    prizeModel.findById(prizeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Data!',
                data: data
            })
        }
    })
 }

 // Update Prize By Id
 const updatePrizeById = (request, response) => {
    const prizeId = request.params.prizeId;
    const body = request.body;

    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'PrizeId is invalid'
        })
    }
    if(!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is required'
        })
    }
    let updatePrize = {
        name: body.name,
        description: body.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
 }

 // Delete Prize
 const deletePrize = (request, response) => {
    const prizeId = request.params.prizeId;
    
    //
    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'PrizeId is invalid'
        })
    }
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!'
            })
        }
    })
 }

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrize
}