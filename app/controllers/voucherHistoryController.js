const mongoose = require('mongoose');

const voucherHistoryModel = require('../models/voucherHistoryModel');
const userModel = require('../models/userModel');
const voucherModel = require('../models/voucherModel');

const createVoucherHistory = (request, response) => {
    const body = request.body;

    const newVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        voucher: mongoose.Types.ObjectId(),
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Create new voucher history',
                data: data
            })
        }
    })
}

const getAllVoucherHistory = (request, response) => {
    let userId = request.query.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    voucherHistoryModel.find(condition)
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Error 500: Internal server error',
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: 'Success: Get all voucher history',
                    data: data
                })
            }
        })
}

const getVoucherHistoryById = (request, response) => {
    const voucherHistoryId = request.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Voucher History ID is not valid'
        })
    }

    voucherHistoryModel.findById(voucherHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: `Success: Get voucher history id ${voucherHistoryId}`,
                data: data
            })
        }
    })
}

const updateVoucherHistory = (request, response) => {
    const voucherHistoryId = request.params.voucherHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Voucher History Id is not valid'
        })
    }

    const updateVoucherHistory = {
        user: body.user,
        voucher: body.voucher
    }

    voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, updateVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: `Success: Update voucher history id ${voucherHistoryId}`,
                data: data
            })
        }
    })
}

const deleteVoucherHistory = (request, response) => {
    const voucherHistoryId = request.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Voucher History ID is not valid'
        })
    }

    voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (error) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: `Success: Delete voucher history id ${voucherHistoryId}`,
            })
        }
    })
}

const createVoucherHistoryForUser = (request, response) => {
    const body = request.body;
    const username = request.query.username;

    voucherModel.count().exec((err, count) => {
        var random = Math.floor(Math.random() * count);
        voucherModel.findOne()
            .skip(random)
            .exec((err, result) => {
                if (err) {
                    return response.status(400).json({
                        status: 'Bad request',
                        message: err.message
                    })
                }
                const newVoucherHistory = {
                    _id: mongoose.Types.ObjectId(),
                    username: username,
                    voucher: result.code,
                    createdAt: body.createdAt,
                    updatedAt: body.updatedAt
                }
                if (username) {
                    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
                        if (error) {
                            return response.status(500).json({
                                status: 'Internal server error',
                                message: error.message
                            })
                        }
                        userModel.findOneAndUpdate({ username }, {
                            $push: {
                                voucherhistory: data.voucher
                            }
                        }, (err, updateUser) => {
                            if (err) {
                                return response.status(500).json({
                                    status: 'Internal server error',
                                    message: error.message
                                })
                            } else {
                                return response.status(201).json({
                                    status: 'Create voucher history successfully',
                                    data: data
                                })
                            }
                        })
                    })
                }
                else {
                    const newVoucherHistory = {
                        _id: mongoose.Types.ObjectId(),
                        username: mongoose.Types.ObjectId(),
                        voucher: result.code,
                        createdAt: body.createdAt,
                        updatedAt: body.updatedAt
                    }
                    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
                        if (error) {
                            return response.status(500).json({
                                status: 'Internal server error',
                                message: error.message
                            })
                        } else {
                            return response.status(201).json({
                                status: 'Create voucher history successfully',
                                data: data
                            })
                        }
                    })
                }
            })
    })
}

const getVoucherHistoryByUsername = (request, response) => {
    const username = request.query.username;

    if(username) {
        userModel.findOne({username}, (errorFindUser, userExist) => {
            if(errorFindUser) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            } else {
                if(!userExist) {
                    return response.status(400).json({
                        status: 'Username is not valid',
                        data: []
                    })
                } else {
                    userModel.findOne({username}, (error, data) => {
                        if(error) {
                            return response.status(500).json({
                                status: 'Internal server error',
                                message: error.message
                            })
                        } else {
                            return response.status(200).json({
                                status: `Get voucher history of user ${username} successfully`,
                                voucherhistory: data.voucherhistory
                            })
                        }
                    })
                }
            }
        })
    }

    if(username == undefined) {
        voucherHistoryModel.find((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: 'Get all vouchers history successfully',
                    data: data
                })
            }
        })
    }
}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistory,
    deleteVoucherHistory,
    createVoucherHistoryForUser,
    getVoucherHistoryByUsername
}