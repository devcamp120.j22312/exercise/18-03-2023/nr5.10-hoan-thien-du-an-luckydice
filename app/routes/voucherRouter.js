// Khai báo thư viện
const express = require('express');

// Import user Controller
const voucherController = require('../controllers/voucherController');

// Khai báo router
const voucherRouter = express.Router();

// Create User
voucherRouter.post('/vouchers', voucherController.createVoucher);

// Get All User
voucherRouter.get('/vouchers', voucherController.getAllVoucher);

// Get User By Id
voucherRouter.get('/vouchers/:voucherId', voucherController.getVoucherById);

// Update User By Id
voucherRouter.put('/vouchers/:voucherId', voucherController.updateVoucherById);

// Delete User By Id
voucherRouter.delete('/vouchers/:voucherId', voucherController.deleteVoucher);

module.exports = voucherRouter