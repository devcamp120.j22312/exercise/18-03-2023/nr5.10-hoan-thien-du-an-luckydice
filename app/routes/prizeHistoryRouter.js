const express = require('express');

const router = express.Router();

const prizeHistoryController = require('../controllers/prizeHistoryController');

router.post('/prize-histories', prizeHistoryController.createPrizeHistory);
router.get('/prize-histories', prizeHistoryController.getAllPrizeHistory);
router.get('/prize-histories/:prizeHistoryId', prizeHistoryController.getPrizeHistoryById);
router.put('/prize-histories/:prizeHistoryId', prizeHistoryController.updatePrizeHistoryById);
router.delete('/prize-histories/:prizeHistoryId', prizeHistoryController.deletePrizeHistoryById);
router.get('/devcamp-lucky-dice/prize-history?username=:username', prizeHistoryController.getPrizeHistoryByUsername);

module.exports = router;