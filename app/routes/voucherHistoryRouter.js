const express = require('express');

const router = express.Router();

const voucherHistoryController = require('../controllers/voucherHistoryController');

router.post('/voucher-histories', voucherHistoryController.createVoucherHistory);
router.get('/voucher-historires', voucherHistoryController.getAllVoucherHistory);
router.get('/voucher-histories/:voucherHistoryId', voucherHistoryController.getVoucherHistoryById);
router.put('/voucher-histories/:voucherHistoryId', voucherHistoryController.updateVoucherHistory);
router.delete('/voucher-histories/:voucherHistoryId', voucherHistoryController.deleteVoucherHistory);
router.get('/devcamp-lucky-dice/voucher-history?username=:username', voucherHistoryController.getVoucherHistoryByUsername);

module.exports = router;