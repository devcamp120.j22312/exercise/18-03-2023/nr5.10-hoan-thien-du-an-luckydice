const express = require('express');

const router = express.Router();

const diceController = require('../controllers/diceController');

router.post('/devcamp-lucky-dice/dice', diceController.diceHandler);

module.exports = router;
