// 
const express = require('express');

const router = express.Router();

// Import Dice History Controller
const diceHistoryController = require('../controllers/diceHistoryController');

// Create Dice History
router.post('/dice-histories', diceHistoryController.createDiceHistory);

// Get All User
router.get('/dice-histories', diceHistoryController.getAllDiceHistory);

// Get Dice History By Username
router.get('/devcamp-lucky-dice/dice-history?username=:username', diceHistoryController.getDiceHistoryByUsername);

// Get User By Id
router.get('/dice-histories/:diceHistoryId', diceHistoryController.getDiceHistoryById);

// Update User By Id
router.put('/dice-histories/:diceHistoryId', diceHistoryController.updateDiceHistoryById);

// Delete User By Id
router.delete('/dice-histories/:diceHistoryId', diceHistoryController.deleteDiceHistory);

module.exports = router