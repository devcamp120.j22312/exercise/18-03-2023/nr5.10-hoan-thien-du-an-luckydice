// 
const mongoose = require('mongoose');

// 
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
    dicehistory: [{
        type: mongoose.Types.ObjectId,
        ref: 'diceHistory'
    }],
    prizehistory: [{
        type: mongoose.Types.ObjectId,
        ref: 'prizehistory'
    }]
})
module.exports = mongoose.model("Users", userSchema);