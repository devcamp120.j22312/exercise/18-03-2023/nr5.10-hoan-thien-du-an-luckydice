// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo Schema
const Schema = mongoose.Schema;

//
const diceHistorySchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    user: {
        type: String,
        ref: "Users",
    },
    dice: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("DiceHistory", diceHistorySchema);