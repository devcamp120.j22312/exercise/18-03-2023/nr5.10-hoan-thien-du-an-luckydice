const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const prizeHistorySchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    username: {
        type: String,
        ref: "Users"
    },
    prize: {
        type: String,
        ref: "Prize"
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('PrizeHistory', prizeHistorySchema);